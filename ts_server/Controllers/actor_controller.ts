import { Router , Request, Response, NextFunction } from 'express';
import { connect } from '../Connections/movie_db';
import { ActorModel, Actor } from '../Models/actor_model';



export namespace ActorController
{
    export async function getAll(req: Request, res: Response, next: NextFunction)
    {
        const results = await ActorModel.getAll();
        res.json(results);
    }

    export async function getByID(req: Request, res: Response, next: NextFunction) //NextFunction -> si on veut utiliser la fonction suivante
    {
        const results = await ActorModel.getByID(req.params.id);
        res.json(results);
    }


    export async function insertActor(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const results = await ActorModel.insertActor(req.body);

            res.json(results);

            const liste = await ActorModel.getAll();

            res.json(liste);

        } catch (err)
        {
            res.status(500).send(err);
        }
    }


    


}

