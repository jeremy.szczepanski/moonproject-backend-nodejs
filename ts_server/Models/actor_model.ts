import { connect } from '../Connections/movie_db';


export class Actor
{
    id: number;
    first_name: string;
    last_name: string;

    constructor(data: any)
    {
        this.id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
    }
}



export class ActorModel
{
    public static async getAll()
    {
         return connect().then((conn) => 
        {
            return conn.query('SELECT * FROM actor').then((results) =>
                {
                    return results;
                });
        });
    }


    public static async getByID(id)
    {
        return connect().then((conn) =>
        {
            return conn.query('SELECT * FROM actor WHERE id=?', id).then((results) =>
            {
                return results;
            });
        });
    }


    public static async insertActor(actor: Actor)
    {
        return connect().then((conn: any) =>
            {
                return conn.query('INSERT INTO actor (first_name, last_name) VALUES(? , ?)', [actor.first_name, actor.last_name]).then
                (
                    (result: any) =>
                    {
                        return this.getAll
                    }
                );
            }
        )
    }


}