import { Router } from 'express';
import { ActorController } from '../Controllers/actor_controller';

export class ActorRouter
{
    public router: Router;


    constructor()
    {
        this.router = Router();

        this.router.get('/', ActorController.getAll);
        this.router.get('/id/:id', ActorController.getByID);
        this.router.post('/create', ActorController.insertActor);
        
    }
}

